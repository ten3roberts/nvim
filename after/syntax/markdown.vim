syn match    customHeader1     "^# .*"
syn match    customHeader2     "^## .*"
syn match    customHeader3     "^### .*"
syn match    customHeader4     "^#### .*"
syn match    customHeader5     "^##### .*"
syn match    customHeader6     "^###### .*"

highlight customHeader6 guifg=#9c4f96
highlight customHeader5 guifg=#ff6355
highlight customHeader4 guifg=#fba949
highlight customHeader3 guifg=#fae442
highlight customHeader2 guifg=#8bd448
highlight customHeader1 guifg=#2aa8f2
